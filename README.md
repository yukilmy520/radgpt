## RadGPT

Your personal code instructor 

RadGPT, the top AI-based web application, offers an exquisite user interface similar to that of the ChatGPT app, seamless communication with the advanced GPT3 model API, and most significantly, the capability to seek assistance from the AI on Python, JavaScript, React, or any other programming language. In addition, RadGPT can convert provided code into another programming language and provides numerous other features.

## Design


## Intended Market

Code Instructor RadGPT is an online AI Chatplatform that helps fixing every coder bugs by just typing into your code. 

## Functionality

- As a user you can ask RadGPT any coding question you may have.

## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Clone the repository down to your local machine
2. CD into the new project directory


